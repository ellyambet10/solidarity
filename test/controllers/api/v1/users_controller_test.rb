require 'test_helper'

class Api::V1::UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:budah)
  end

  test "should get index" do
    get api_v1_users_url, as: :json
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post api_v1_users_url, params: { user: { name: 'John Doe', id_number: '32155555', phone_number: '+254703103579', age: '22', gender: 'male', occupation: 'engineer', location: 'Nairobi', union: 'TSC', password: 'password' } }, as: :json
    end

    assert_response 201
  end

  test "a user can be created with the correct credentials" do
    assert_difference('User.count') do
      post api_v1_users_url, params: { user: { name: 'John Doe', id_number: '32155555', phone_number: '+254703103579', age: '22', gender: 'male', occupation: 'engineer', location: 'Nairobi', union: 'TSC', password: 'password' } }, as: :json
    end
    assert_response 201
    assert assigns(:user).name, 'John Doe'
    assert assigns(:user).phone_number, '+254703103579'
    assert assigns(:user).id_number, '32155555'
    assert assigns(:user).age, '22'
    assert assigns(:user).gender, 'male'
    assert assigns(:user).occupation, 'engineer'
    assert assigns(:user).location, 'Nairobi'
    assert assigns(:user).union, 'TSC'
    refute_nil assigns(:user).phone_verification_code
  end
=begin
  test 'a user created will enqueue a verification SMS' do
    assert_difference('ActiveJob::Base.queue_adapter.enqueued_jobs.count') do
      post api_v1_users_url, params: { user: { name: 'John Doe', id_number: '32155555', phone_number: '+254703103579', age: '22', gender: 'male', occupation: 'engineer', location: 'Nairobi', union: 'TSC', password: 'password' } }, as: :json
    end

    assert_response 201
  end
=end
  test 'a created user can verify their phone number with the correct code' do
    patch api_v1_user_verify_phone_number_url(@user),
      params:  {
        id: @user.id,
        user: {
          verification_code: '1234'
        }
      }, as: :json

    #refute_nil json_response.dig('user','jwt'), 'JWT token not created'
    assert_response 200
    assert @user.reload.verified_phone?
  end

  test 'a created user cannot verify their phone number with the incorrect code' do
    patch api_v1_user_verify_phone_number_url(@user),
      params:  {
        id: @user.id,
        user: {
          verification_code: '9876'
        }
      }, as: :json
    assert_response 422
    refute @user.reload.verified_phone?
    assert json_response.dig('verification_code').include?("Incorrect Verification Code")
  end

  test "should show user" do
    get api_v1_user_url(@user), as: :json
    assert_response :success
  end
=begin
  test "should update user" do
    patch api_v1_users_url(@user), params: { user: { name: 'John Doe', id_number: '32155555', phone_number: '+254703103579', age: '22', gender: 'male', occupation: 'engineer', location: 'Nairobi', union: 'TSC', password: 'password' } }, as: :json
    assert_response 200
  end
=end
  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete api_v1_user_url(@user), as: :json
    end

    assert_response 204
  end
end
