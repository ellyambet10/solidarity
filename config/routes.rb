Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      #post 'sessions/create'
      resources :users do
        patch :verify_phone_number
      end
    end
  end
  root 'welcome#index'
end