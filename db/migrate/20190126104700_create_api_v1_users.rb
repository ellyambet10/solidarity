class CreateApiV1Users < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :id_number, null: false
      t.string :phone_number, null: false
      t.string :age, null: false
      t.string :gender, null: false
      t.string :occupation, null: false
      t.string :location, null: false
      t.string :union, null: false
      t.string :password_digest
      t.integer :user_type, default: 2, null: false, index: true
      t.string :phone_verification_code
      t.boolean :verified_phone, default: false
      t.boolean :active, default: false
      t.datetime :last_login

      t.timestamps
    end
  end
end
