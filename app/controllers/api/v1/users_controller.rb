class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy, :verify_phone_number]

  def index
    @users = User.all

    render json: @users
  end

  def show
    render json: @user
  end

  def create
    @user = User.new(user_params)

    if @user.save
      SendVerificationMessageJob.perform_later @user
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @user.destroy
  end

  def verify_phone_number
    if @user.correct_verification_code?(params.dig('user','verification_code'))
      @user.mark_phone_verified!
      #jwt = Auth.issue({user: @user.fingerprint, expires_at: 25.minutes.from_now})
      #render json: @user, jwt: jwt , message: 'User phone number was successfully verified.', status: :ok
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.fetch(:user, {})
      params.require(:user).permit(:name, :id_number, :phone_number, :age, :gender, :occupation, :location, :union, :password, :user_type)
    end
end
