class User < ApplicationRecord
  has_secure_password

  before_create :create_verification_code

  validates :name, presence: true
  validates :id_number, presence: true
  validates :phone_number, uniqueness: true, presence: true
  validates :age, presence: true
  validates :gender, presence: true
  validates :occupation, presence: true
  validates :location, presence: true
  validates :union, presence: true

  enum user_type: { admin: 0, agent: 1, client: 2 }

  def correct_verification_code?(verification_code)
    if verification_code == phone_verification_code
      return true
    else
      errors.add(:verification_code, I18n.t('users.wrong_verification_code'))
      return false
    end
  end

  def mark_phone_verified!
    toggle!(:verified_phone) unless verified_phone
  end

  private

  def create_verification_code
    self.phone_verification_code = rand.to_s[2..5]
  end

end
