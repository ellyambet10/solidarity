class SendVerificationMessageJob < ApplicationJob
  queue_as :default

  def perform(user)
    SendVerificationMessage.new(user).call
  end
end
