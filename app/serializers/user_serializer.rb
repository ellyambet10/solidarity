class UserSerializer < ActiveModel::Serializer
  attributes :id,
  :name,
  :id_number,
  :phone_number,
  :age,
  :gender,
  :occupation,
  :location,
  :union,
  :user_type,
  :verified_phone,
  :active,
  :last_login,
  :created_at,
  :updated_at
=begin
  def jwt
    instance_options[:jwt]
  end
=end
end
