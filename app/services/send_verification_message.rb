class SendVerificationMessage

  def initialize(user)
    @user = user
  end

  def call
    client = Twilio::REST::Client.new
    client.messages.create({
      from: ENV['TWILIO_NUMBER'],
      to: @user.phone_number,
      body: @user.phone_verification_code
    })
  end
end
